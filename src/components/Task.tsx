'use client'
import { CheckCircle, Circle, PlusCircle, Trash } from "@phosphor-icons/react";
import { useState } from "react";

import ClipBoardImage from '../../public/images/Clipboard.svg'
import Image from "next/image";

interface TaskProps {
  id: number;
  description: string;
  completed: boolean;
}

export default function Task() {
  const [newTask, setNewTask] = useState("");
  const [taskCompleted, setTaskCompleted] = useState(0);
  const [createdTasks, setCreatedTasks] = useState(0)
  const [taskArray, setTaskArray] = useState<TaskProps[]>([]);

  function handleCompleteTask(id: number) {
    const updatedTasks = taskArray.map((task) =>
      task.id === id ? { ...task, completed: !task.completed } : task
    );
    setTaskCompleted(taskCompleted + 1)
    setTaskArray(updatedTasks);
  }
  function handleIncompleteTask(id: number) {
    const updatedTasks = taskArray.map((task) =>
      task.id === id ? { ...task, completed: !task.completed } : task
    );
    setTaskCompleted(taskCompleted - 1)
    setTaskArray(updatedTasks);
  }

  function removeTask(id: number) {
    const taskToRemove = taskArray.find((task) => task.id === id);
  
    if (taskToRemove) {
      // Verifique se a tarefa a ser removida está completa
      if (taskToRemove.completed) {
        setTaskCompleted(taskCompleted - 1);
      }
  
      // Remova a tarefa do array
      const updatedTasks = taskArray.filter((task) => task.id !== id);
      setTaskArray(updatedTasks);
  
      // Decrementar createdTasks
      setCreatedTasks(createdTasks - 1);
    }
  }
  

  function handleCreateNewTask(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();

    if (newTask.trim() === "") {
      return; // Não adiciona tarefas em branco
    }

    const newItem = {
      id: taskArray.length + 1, // ID sequencial
      description: newTask,
      completed: false,
    };

    setTaskArray([...taskArray, newItem]);
    setCreatedTasks(createdTasks + 1);
    setNewTask(""); // Limpa o campo de entrada
  }

  function handleCreateNewTaskChange(event: React.ChangeEvent<HTMLInputElement>) {
    event.preventDefault();
    setNewTask(event.target.value);
  }

  return (
    <main className="w-full max-w-3xl flex flex-col m-auto">
      <form onClick={handleCreateNewTask}>
        <div className="w-full h-14 flex gap-4 -mt-7">
          <input
            className="w-full bg-gray400 p-4 border-none placeholder:text-gray300 text-gray200 rounded-lg focus:outline-none focus:ring-1 focus:ring-purpleDark"
            type="text"
            name="task"
            value={newTask}
            onChange={handleCreateNewTaskChange}
            placeholder="Adicione uma tarefa"
          />
          <button className=" flex items-center gap-2 p-4 bg-blueDark text-gray100 text-sm font-bold rounded-lg hover:bg-blue transition ease-in" type="submit">
            Criar
            <PlusCircle size={20} weight="regular" />
          </button>
        </div>
      </form>

      <div className="w-full mt-8">
        {createdTasks == 0 &&
          <div>
            <div className="h-10 flex justify-between text-gray100 text-sm font-bold">
              <span className="flex gap-4">Tarefas criadas <strong>0</strong></span>
              <span className="flex gap-4">Concluídas <strong>0</strong></span>
            </div>

            <div className="w-full h-60 flex flex-col items-center justify-center shadow-[0px_-.3px_0px_0px_#808080] rounded-t-lg">
              <Image src={ClipBoardImage} alt="" />
              <p className="w-80 flex flex-col items-center mt-4 text-gray300 text-base">
                <strong>Você ainda não tem tarefas cadastradas</strong>
                Crie tarefas e organize seus itens a fazer
              </p>
            </div>
          </div>
        }

        {createdTasks > 0 &&
          <div>
            <div className="h-10 flex items-center justify-between text-gray100 text-sm font-bold">
              <span className="flex gap-4 text-blue">
                Tarefas criadas
                <p className="px-2 py-[2px] rounded-full bg-gray400 font-bold text-xs text-gray100">{createdTasks}</p>
              </span>
              <span className="flex gap-4 text-purple">
                Concluídas
                <p className="px-2 py-[2px] rounded-full bg-gray400 font-bold text-xs text-gray100">{taskCompleted} de {createdTasks}</p>
              </span>
            </div>

            {/* Tasks */}
            <div className="w-full h-[500px] flex flex-col gap-3 rounded-lg">
              {taskArray.map((data) => {
                return (
                  <div key={data.id} className="w-full flex justify-start gap-3 p-4 bg-gray400 rounded-lg">
                    {!data.completed ? (
                      <Circle
                        className="hover:cursor-pointer text-blue hover:text-blueDark transition-colors ease-in"
                        size={24}
                        weight="regular"
                        onClick={() => handleCompleteTask(data.id)}
                      />
                    ) : (
                      <CheckCircle
                        className="hover:cursor-pointer fill-purple hover:fill-purpleDark transition-colors ease-in"
                        size={24}
                        onClick={() => handleIncompleteTask(data.id)}
                      />
                    )}
                    <p
                      className={`w-full text-sm ${data.completed
                        ? 'line-through opacity-70 text-gray-300'
                        : 'text-gray-100'
                        } pr-10 leading-6`}
                    >
                      {data.description}
                    </p>
                    <Trash
                      className="hover:cursor-pointer text-gray-300 hover:text-danger transition-colors ease-in"
                      size={24}
                      onClick={() => removeTask(data.id)}
                    />
                  </div>
                );
              })}
            </div>
          </div>
        }
      </div>
    </main>
  )
}