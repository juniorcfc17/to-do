import './globals.css'
import type { Metadata } from 'next'
import { Inter } from 'next/font/google'
import Image from 'next/image'

import logo from '../../public/images/Logo.svg'

const inter = Inter({ subsets: ['latin'] })

export const metadata: Metadata = {
  title: 'To Do',
}

export default function RootLayout({ children }: {children: React.ReactNode}) {
  return (
    <html lang="pt-br">
      <body className={`h-full flex flex-col relative bg-gray500 ${inter.className}`}>
        <header className='h-52 w-full flex items-center justify-center bg-gray700'>
          <Image src={logo} alt=''/>
        </header>
        {children}
      </body>
    </html>
  )
}
