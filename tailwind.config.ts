import type { Config } from 'tailwindcss'

const config: Config = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      backgroundImage: {
        'gradient-radial': 'radial-gradient(var(--tw-gradient-stops))',
        'gradient-conic':
          'conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))',
      },
      colors: {
        purple:     '#8284FA',
        purpleDark: '#5E60CE',

        blue:       '#4EA8DE',
        blueDark:   '#1E6F9F',

        gray700:    '#0D0D0D',
        gray600:    '#1A1A1A',
        gray500:    '#262626',
        gray400:    '#333333',
        gray300:    '#808080',
        gray200:    '#D9D9D9',
        gray100:    '#F2F2F2',   

        danger:     '#E25858',
      }
    },
  },
  plugins: [],
}
export default config
